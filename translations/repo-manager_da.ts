<?xml version="1.0" ?><!DOCTYPE TS><TS language="da" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="ui_mainwindow.h" line="332"/>
        <source>Repo Manager</source>
        <translation>Softwarekildehåndtering</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <location filename="mainwindow.cpp" line="487"/>
        <location filename="ui_mainwindow.h" line="333"/>
        <source>Select the APT repository that you want to use:</source>
        <translation>Vælg den APT-softwarekilde som du vil bruge:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <location filename="ui_mainwindow.h" line="337"/>
        <source>antiX repos</source>
        <translation>antiX-softwarekilder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <location filename="ui_mainwindow.h" line="334"/>
        <source>Select fastest antiX repo for me</source>
        <translation>Vælg hurtigste antiX-softwarekilde for mig</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <location filename="ui_mainwindow.h" line="336"/>
        <source>search</source>
        <translation>søg</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="ui_mainwindow.h" line="339"/>
        <source>Debian repos</source>
        <translation>Debian-softwarekilder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <location filename="ui_mainwindow.h" line="338"/>
        <source>Select fastest Debian repo for me</source>
        <translation>Vælg hurtigste Debian-softwarekilde for mig</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <location filename="ui_mainwindow.h" line="341"/>
        <source>Individual sources</source>
        <translation>Individuelle kilder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="340"/>
        <source>Restore original APT sources</source>
        <translation>Gendan originale APT-kilder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="343"/>
        <source>Display help </source>
        <translation>Vis hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <location filename="ui_mainwindow.h" line="345"/>
        <source>Help</source>
        <translation>Hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <location filename="ui_mainwindow.h" line="346"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <location filename="ui_mainwindow.h" line="348"/>
        <source>About this application</source>
        <translation>Om programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="350"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="351"/>
        <source>Alt+B</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <location filename="ui_mainwindow.h" line="353"/>
        <source>Quit application</source>
        <translation>Afslut programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="422"/>
        <location filename="ui_mainwindow.h" line="355"/>
        <source>Close</source>
        <translation>Luk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="429"/>
        <location filename="ui_mainwindow.h" line="356"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <location filename="ui_mainwindow.h" line="357"/>
        <source>Apply</source>
        <translation>Anvend</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Please wait...</source>
        <translation>Vent venligst...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="356"/>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Success</source>
        <translation>Det lykkedes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <location filename="mainwindow.cpp" line="357"/>
        <location filename="mainwindow.cpp" line="610"/>
        <source>Your new selection will take effect the next time sources are updated.</source>
        <translation>Din nye markering træder i kraft næste gang kilderne opdateres.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Lists</source>
        <translation>Lister</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Sources (checked sources are enabled)</source>
        <translation>Kilder (tjekkede kilder er aktiveret)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="537"/>
        <location filename="mainwindow.cpp" line="548"/>
        <location filename="mainwindow.cpp" line="566"/>
        <location filename="mainwindow.cpp" line="594"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>Could not change the repo.</source>
        <translation>Kunne ikke skifte softwarekilden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>About %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="403"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="404"/>
        <source>Program for choosing the default APT repository</source>
        <translation>Program til at vælge standard-APT-softwarekilden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="406"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Ophavsret (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>%1 Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>You have selected MX Test Repo. It&apos;s not recommended to leave it enabled or to upgrade all the packages from it.</source>
        <translation>Du har valgt MX testsoftwarekilde. Det anbefales ikke at lade den være aktiveret eller at opgradere alle pakkerne fra den.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>A safer option is to install packages individually with MX Package Installer.</source>
        <translation>Et sikre valg er at installere pakker hver for sig med MX Pakkeinstallationsprogram.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>Select the APT repository and sources that you want to use:</source>
        <translation>Vælg den APT-softwarekilde og -kilder som du vil bruge:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="538"/>
        <source>netselect-apt could not detect fastest repo.</source>
        <translation>netselect-apt kunne ikke registrere hurtigste softwarekilde.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="549"/>
        <location filename="mainwindow.cpp" line="567"/>
        <source>Could not detect fastest repo.</source>
        <translation>Kunne ikke registrere hurtigste softwarekilde.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="595"/>
        <source>Could not download original APT files.</source>
        <translation>Kunne ikke downloade de originale APT-filer.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Original APT sources have been restored to the release status. User added source files in /etc/apt/sources.list.d/ have not been touched.</source>
        <translation>De originale APT-kilder er blevet gendannet til udgivelsesstatussen. Kildefiler som er blevet tilføjet af en bruger i /etc/apt/sources.list.d/ er ikke blevet rørt.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Ændringslog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>You must run this program as root.</source>
        <translation>Du skal køre programmet som root.</translation>
    </message>
</context>
</TS>