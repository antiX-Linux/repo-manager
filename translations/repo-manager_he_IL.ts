<?xml version="1.0" ?><!DOCTYPE TS><TS language="he_IL" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="ui_mainwindow.h" line="332"/>
        <source>Repo Manager</source>
        <translation>מנהל מאגרים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <location filename="mainwindow.cpp" line="487"/>
        <location filename="ui_mainwindow.h" line="333"/>
        <source>Select the APT repository that you want to use:</source>
        <translation>נא לבחור את מאגר APT שברצונך להשתמש בו:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <location filename="ui_mainwindow.h" line="337"/>
        <source>antiX repos</source>
        <translation>מאגרים של antiX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <location filename="ui_mainwindow.h" line="334"/>
        <source>Select fastest antiX repo for me</source>
        <translation>תבחר את מאגר הantiX הכי מהיר בשבילי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <location filename="ui_mainwindow.h" line="336"/>
        <source>search</source>
        <translation>חיפוש</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <location filename="ui_mainwindow.h" line="339"/>
        <source>Debian repos</source>
        <translation>מאגרים של Debian</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <location filename="ui_mainwindow.h" line="338"/>
        <source>Select fastest Debian repo for me</source>
        <translation>תבחר את מאגר הDebian הכי מהיר בשבילי</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <location filename="ui_mainwindow.h" line="341"/>
        <source>Individual sources</source>
        <translation>מקורות אישיים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="340"/>
        <source>Restore original APT sources</source>
        <translation>שחזור מקורות APT מקוריים</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <location filename="ui_mainwindow.h" line="343"/>
        <source>Display help </source>
        <translation>הצגת עזרה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="341"/>
        <location filename="ui_mainwindow.h" line="345"/>
        <source>Help</source>
        <translation>עזרה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <location filename="ui_mainwindow.h" line="346"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <location filename="ui_mainwindow.h" line="348"/>
        <source>About this application</source>
        <translation>מידע לגבי התוכנה הזו</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <location filename="ui_mainwindow.h" line="350"/>
        <source>About...</source>
        <translation>אודות...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="351"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <location filename="ui_mainwindow.h" line="353"/>
        <source>Quit application</source>
        <translation>יציאה מהתוכנה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="422"/>
        <location filename="ui_mainwindow.h" line="355"/>
        <source>Close</source>
        <translation>סגור</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="429"/>
        <location filename="ui_mainwindow.h" line="356"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <location filename="ui_mainwindow.h" line="357"/>
        <source>Apply</source>
        <translation>אשר</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="67"/>
        <source>Please wait...</source>
        <translation>חכה בבקשה...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="128"/>
        <location filename="mainwindow.cpp" line="356"/>
        <location filename="mainwindow.cpp" line="608"/>
        <source>Success</source>
        <translation>הצלחה</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <location filename="mainwindow.cpp" line="357"/>
        <location filename="mainwindow.cpp" line="610"/>
        <source>Your new selection will take effect the next time sources are updated.</source>
        <translation>הבחירה שלך תיכנס לתוקף בפעם הבאה שהמקורות יתעדכנו.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Lists</source>
        <translation>רשימות</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Sources (checked sources are enabled)</source>
        <translation>מקורות (המקורות הנבחרים מאופשרים)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="537"/>
        <location filename="mainwindow.cpp" line="548"/>
        <location filename="mainwindow.cpp" line="566"/>
        <location filename="mainwindow.cpp" line="594"/>
        <source>Error</source>
        <translation>תקלה</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="360"/>
        <source>Could not change the repo.</source>
        <translation>שינוי המאגר לא הצליח.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>About %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="403"/>
        <source>Version: </source>
        <translation>גרסה:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="404"/>
        <source>Program for choosing the default APT repository</source>
        <translation>תכנית לבחירת מאגר APT ברירת המחדל</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="406"/>
        <source>Copyright (c) MX Linux</source>
        <translation>זכויות שמורות MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 License</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="422"/>
        <source>%1 Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>Warning</source>
        <translation>אזהרה</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>You have selected MX Test Repo. It&apos;s not recommended to leave it enabled or to upgrade all the packages from it.</source>
        <translation>בחרת במאגר נסיוני MX Test Repo. לא מומלץ להשאיר אותו מאופשר או לעדכן את כל החבילות ממנו.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>A safer option is to install packages individually with MX Package Installer.</source>
        <translation>אופצייה בטוחה יותר היא להתקין חבילות בנפרד עם מתקין החבילות של MX</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>Select the APT repository and sources that you want to use:</source>
        <translation>נא לבחור את מאגר APT והמקורות שברצונך להשתמש בהם:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="538"/>
        <source>netselect-apt could not detect fastest repo.</source>
        <translation>netselect-apt לא הצליח למצוא את המאגר המהיר ביותר</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="549"/>
        <location filename="mainwindow.cpp" line="567"/>
        <source>Could not detect fastest repo.</source>
        <translation>המאגר המהיר ביותר לא נמצא</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="595"/>
        <source>Could not download original APT files.</source>
        <translation>הורדה של קבצי APT מקוריים לא הצליחה</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Original APT sources have been restored to the release status. User added source files in /etc/apt/sources.list.d/ have not been touched.</source>
        <translation>מקורות APT מקוריים שוחזרו למצב שחרור. קבצי המקור ב/etc/apt/sources.list.d/ לא שונו.</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>רשיון</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>יומן שינויים</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;סגירה</translation>
    </message>
    <message>
        <location filename="main.cpp" line="53"/>
        <source>You must run this program as root.</source>
        <translation>יש להריץ את התכנית הזו כמשתמש על.</translation>
    </message>
</context>
</TS>