# **********************************************************************
# * Copyright (C) 2016 MX Authors
# *
# * Authors: Adrian
# *          MX Linux <http://mxlinux.org>
# *
# * This file is part of repo-manager.
# *
# * repo-manager is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * repo-manager is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with repo-manager.  If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

QT       += core gui

CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = repo-manager
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    cmd.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    version.h \
    cmd.h \
    about.h

FORMS    += mainwindow.ui

TRANSLATIONS += translations/repo-manager_am.ts \
                translations/repo-manager_ar.ts \
                translations/repo-manager_bg.ts \
                translations/repo-manager_bn.ts \
                translations/repo-manager_ca.ts \
                translations/repo-manager_cs.ts \
                translations/repo-manager_da.ts \
                translations/repo-manager_de.ts \
                translations/repo-manager_el.ts \
                translations/repo-manager_es.ts \
                translations/repo-manager_et.ts \
                translations/repo-manager_eu.ts \
                translations/repo-manager_fa.ts \
                translations/repo-manager_fil_PH.ts \
                translations/repo-manager_fi.ts \
                translations/repo-manager_fr.ts \
                translations/repo-manager_he_IL.ts \
                translations/repo-manager_hi.ts \
                translations/repo-manager_hr.ts \
                translations/repo-manager_hu.ts \
                translations/repo-manager_id.ts \
                translations/repo-manager_is.ts \
                translations/repo-manager_it.ts \
                translations/repo-manager_ja.ts \
                translations/repo-manager_kk.ts \
                translations/repo-manager_ko.ts \
                translations/repo-manager_lt.ts \
                translations/repo-manager_mk.ts \
                translations/repo-manager_mr.ts \
                translations/repo-manager_nb.ts \
                translations/repo-manager_nl.ts \
                translations/repo-manager_pl.ts \
                translations/repo-manager_pt.ts \
                translations/repo-manager_pt_BR.ts \
                translations/repo-manager_ro.ts \
                translations/repo-manager_ru.ts \
                translations/repo-manager_sk.ts \
                translations/repo-manager_sl.ts \
                translations/repo-manager_sq.ts \
                translations/repo-manager_sr.ts \
                translations/repo-manager_sv.ts \
                translations/repo-manager_tr.ts \
                translations/repo-manager_uk.ts \
                translations/repo-manager_zh_CN.ts \
                translations/repo-manager_zh_TW.ts

RESOURCES += \
    images.qrc
